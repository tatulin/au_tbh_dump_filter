# -*- coding: UTF-8 -*-
import codecs, sys

def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f


def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

fn = sys.argv[1]
limit = 10000
with utf_open_read(fn) as f, utf_open_write(fn+"__head{}.txt".format(str(limit))) as out_f:
    lc = 0
    for line in f:
        lc+=1
        if lc>limit:
            break
        out_f.write(line)
print "Done"
