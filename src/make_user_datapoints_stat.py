# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time

from log_processor import * #LogProcessor

help_doc = u"""
Скрипт составляет кол-во датапоинтов и, кол-во пользователей с таким кол-вом датапоинтов.
Выводит данные ввиде: кол-во датапоинтов - кол-во пользователей
Выходной файл создается в той же директории с именем "имя_входного_файла"+"_make_user_dps.csv"
"""

"""
class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass
"""


class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        self.dps_users_dict = {} #{ numberOfDatapoints: [numberOfUser]}
    def process_tokens(self, tokens_list):
        all_user_datapoints = {}   #userID: numberOfDatapoints
        for tokens in tokens_list:
            userId = tokens[0]
            if userId.strip() == "":
                continue
            if userId not in all_user_datapoints:
                all_user_datapoints[userId] = 1
            else:
                all_user_datapoints[userId] += 1
        pass


        items = all_user_datapoints.items()
        for el in items:
            numberOfDatapoints = el[1]
            if numberOfDatapoints not in self.dps_users_dict:
                self.dps_users_dict[numberOfDatapoints] = 1
            else:
                self.dps_users_dict[numberOfDatapoints] += 1
        pass

        """for key in all_user_datapoints:
            numberOfDatapoints = all_user_datapoints[key]
            if numberOfDatapoints not in self.dps_users_dict:
                self.dps_users_dict[numberOfDatapoints] = 1
            else:
                self.dps_users_dict[numberOfDatapoints] += 1
        pass"""




    def save_dict(self):
        out_fn = self.fn + "_make_user_dps.csv"
        du = self.dps_users_dict.items()
        du.sort(key=lambda x: -int(x[0]))
        with utf_open_write(out_fn) as out_f:
            out_f.write('\t'.join(['number of datapoints','number of users']) + '\r')
            for el in du:
                new_line = "\t".join([str(el[0]), str(el[1])])
                out_f.write(new_line + "\r")
            pass
        pass


def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass

if __name__ == '__main__':
    main()
    pass
