# -*- coding: UTF-8 -*-
import codecs
import sys, os, time

help_doc = u"""
Пропускает через себя лог, запускает аккумуляторы, сохраняет результат.
"""
def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

def ts():
    return time.strftime("%H:%M:%S", time.localtime())


fn = sys.argv[1]
out_fn = fn + "_dict.csv"
d = {}
f =  utf_open_read(fn)
for line in f:
    parts = line.strip().split(".")
    dm = parts[-1]
    if dm=="":
        continue
    if dm not in d:
        d[dm] = 1
    else:
        d[dm] += 1
f.close()

#save
els = d.items()
o_f = utf_open_write(out_fn)
for el in els:
    new_line  = el[0] + "\t" + str(el[1]) + "\r"
    o_f.write(new_line)
o_f.close()