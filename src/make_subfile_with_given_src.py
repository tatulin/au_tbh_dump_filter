# -*- coding: UTF-8 -*-
import codecs
import sys, os


def print_help():
    r = u"""
Скрипт выгружает из входного файла датапойнты с поставщиком, заданным а аргументах командной строки

Первый аргумент командной строки - файл для исследования.
Второй аргумент - строка поставщика. Например: log_matching_hotlog
Третий (опциональный) - файл, куда выгружать.
Если третий аргумент не задан, то просто к имени входного файла приплюсутся имя поставщика.

Запуск скрипта баз аргументов выводит это сообщение.

"""
    print r
    return r

def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

def process(input_fn, source, output_fn):
    lc = 0
    with utf_open_read(input_fn) as f, utf_open_write(output_fn) as out_f:
        for line in f:
            lc += 1
            if lc%100000==0:
                print lc/1000, "k lines processed"
            tokens = line.strip().split("\t")
            if len(tokens)<>17:
                continue
            line_src = tokens[3]
            if line_src==source:
                out_f.write(line)

    pass

def main():
    """ Агрументы командной строки наяинаются с нуля Нулевой аргумент - имя файла запущенного скрипта."""
    arglen = len(sys.argv)
    if arglen <3:
        print_help()
        return
    fn = sys.argv[1]
    src_name = sys.argv[2]
    if arglen<4:
        out_fn = fn + src_name + '.csv'
    else:
        out_fn = sys.argv[3]

    print u"Входной файл:", fn
    print u"Поставщик:", src_name
    print u"Файл с готовой выгрузкой:", out_fn

    process(fn, src_name, out_fn)

    pass

if __name__ == '__main__':
    main()
    pass