# -*- coding: UTF-8 -*-
import codecs
import sys, os, time, math, time, progress_printer

from log_processor import * #LogProcessor

help_doc = u"""
Скрипт фильтрует файл подающийся на вход и выводит отфильтрованый результ в новый файл.
Фильтрация производится по попаданию максимального числа датапоинтов за один день(у одного uID может быть история за несколько дней, скрипт выбирает 1 день с максимальным кол-вом датапоинтов) по одному uID,
в промежуток задающийся параметрами gte и lte [gte:lte] и также фильтрует по длине истории пользователя(кол-во дней):если история < min days, кука не пишется в файл.
Переменные gte и lte объявляются в функции main()
Выходной файл создается в той же директории с именем "имя_входного_файла"+"_dpc_gte{}_dps_lte{}_min_days{}.csv".format(str(self.gte), str(self.lte), str(self.min_days))
"""


class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass


class ThemeAcc:
    def __init__(self, fn, gte=None, lte=None, min_dtps=None, min_days=None, max_days=None):
        self.fn = fn
        self. d = {}
        self.gte = gte  #от gte датапоинтов за день
        self.lte = lte  #до lte датапоинтов за день
        self.min_dtps = min_dtps  #минимальное кол-во датапоинтов по одной куке, при которой будет работать основной скрипт
        self.min_days = min_days
        self.max_days = max_days
        self.out_fn = fn + "_dpc_gte{}_dps_lte{}_min_days{}.csv".format(str(self.gte), str(self.lte), str(self.min_days))

        self.out_file = utf_open_append(self.out_fn)
        self.wo_double_dict = {}

    def __del__(self):
        pass

    def process_tokens(self, tokens_list):
        dict_dtps_by_day = {}

        if self.min_dtps:
            if len(tokens_list) < self.min_dtps:
                return

        for item in tokens_list:
            date = item[4][:10].strip()
            if date not in dict_dtps_by_day:
               dict_dtps_by_day[date] = 1
            else:
               dict_dtps_by_day[date] += 1
            pass

        if self.min_days:
            if len(dict_dtps_by_day.keys()) < self.min_days:
                return

        if self.max_days:
            if len(dict_dtps_by_day.keys()) > self.max_days:
                return

        dict_dtps_by_day_values = dict_dtps_by_day.values()
        dict_dtps_by_day_values.sort(key=lambda x: -x)
        max_dtps = dict_dtps_by_day_values[0]

        if self.gte:
            if max_dtps < self.gte:
                return

        if self.lte:
            if max_dtps > self.lte:
                return

        for item in tokens_list:
            self.out_file.write("\t".join(item) + "\r")




    def save_dict(self):
        pass


def main():
    gte = 2
    lte = 500
    min_dtps = 1
    min_days = 1
    max_days = 20
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn, gte, lte, min_dtps, min_days, max_days)
    LogProcessor().process_log(fn, [ta])
    print ts(), u"Закончено"
    print u"путь файла: ", ta.out_fn

    pass

if __name__ == '__main__':
    main()
    pass