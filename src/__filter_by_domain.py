# -*- coding: UTF-8 -*-
import codecs
import sys, os, time


help_doc = u"""
Пропускает через себя лог, оставляет только один домен
"""
domain_filter = "medi.ru"


def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f


def main():
    fn = sys.argv[1]
    out_fn = fn + "_filtered_by_domain.csv"

    with utf_open_read(fn) as f, utf_open_write(out_fn) as out_f:
        for line in f:
            tokens = line.strip().split("\t")
            if len(tokens)<11: # tokens[10] = ash
                continue
            domain = tokens[2]
            if domain_filter in domain:
                out_f.write(line)
    print u"Завершено."
    print u"Фильтр по домену и субдоменам", domain_filter
    print u"создан файл " ,out_fn
    time.sleep(30)


if __name__ == '__main__':
    main()
    pass