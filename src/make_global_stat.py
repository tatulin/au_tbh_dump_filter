# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time, math, time


from log_processor import * #LogProcessor
help_doc = u"""
Скрипт выводит статистику по файлу

"""

class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass



class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        out_fn = fn+"_global_stat.csv"
        self.number_datapoints = 0
        self.number_users = 0
        self.number_datapoints_5 = 0
        self.number_users_5 = 0
        self.out_file = utf_open_append(out_fn)
        self.wo_double_dict = {}

    def __del__(self):
        pass

    def process_tokens(self, tokens_list):
        dp_count = len(tokens_list)
        self.number_users += 1
        self.number_datapoints += dp_count
        if dp_count > 5:
            self.number_users_5 += 1
            self.number_datapoints_5 += dp_count

    def print_stat(self):
        self.out_file.write(u"всего датапоинтов = {} \r всего юзеров = {} \r ".format(str(self.number_datapoints),\
                                                                                      str(self.number_users)))
        self.out_file.write(u"всего датапоинтов 5+ = {} \r всего юзеров 5+ = {}".format(str(self.number_datapoints_5),\
                                                                                        str(self.number_users_5)))
        self.out_file.close()




    def save_dict(self):
        pass


def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    ta.print_stat()
    print ts(), u"Закончено"

    pass

if __name__ == '__main__':
    main()
    time.sleep(3600)
    pass
