# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time, math


from log_processor import * #LogProcessor


help_doc = u"""
Скрипт составляет список числа дней(длина истории пользовател), кол-во пользователей с такой длиной истории.
Выводит данные ввиде: число дней - кол-во пользователей
Выходной файл создается в той же директории с именем "имя_входного_файла"+"_len_history.csv"
"""


class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        self.days_users_dict = {} #{ numberOfDays: [numberOfUser]}
    def process_tokens(self, tokens_list):
        days = []#множество неповторяющихся дат(yyyy-mm-dd) по одному uid

        for tokens in tokens_list:
            userID = tokens[0]
            date = tokens[4][:10].strip()#обрезает строку формата [yyyy-mm-dd hhhh-mm-ss] до [yyyy-mm-dd]
            days.append(date)
        pass
        days = set(days)

        lenHistory = len(days)
        if lenHistory not in self.days_users_dict:
            self.days_users_dict[lenHistory] = 1
        else:
            self.days_users_dict[lenHistory] += 1
        pass

    def save_dict(self):
        out_fn = self.fn + "_len_history.csv"
        du = self.days_users_dict.items()
        du.sort(key=lambda x: -int(x[0]))
        with utf_open_write(out_fn) as out_f:
            out_f.write('\t'.join(['number of days','number of users']) + '\r')
            for el in du:
                new_line = "\t".join([str(el[0]), str(el[1])])
                out_f.write(new_line + "\r")
            pass
        pass

def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass

if __name__ == '__main__':
    main()
    pass