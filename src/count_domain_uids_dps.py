# -*- coding: UTF-8 -*-
import codecs
import sys, os, time

help_doc = u"""
Требуется аргумент командной строки - путь к файлу дампа.
Будет создан файл со статистикой  - домен - количество униальных пользователей- кол-во датапойнтов

При запуске без аргументов или с более, чем одним аргументом, будет выведено это сообщение.
"""

def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

def ts():
    return time.strftime("%H:%M:%S", time.localtime())

class DomainUIDsDPs():
    def __init__(self, fn):
        self.domain_uids = {}
        self.domain_dps = {}
        self.domain_uids_dps = {} #{ domain: [uids, dps]}
        self.fn=fn


    def process_tokens(self, tokens_list):
        all_user_domains = {} #domain: urls_visited
        for tokens in tokens_list:
            domain = tokens[2]
            if domain.strip()=="":
                continue
            if domain not in self.domain_dps:
                self.domain_dps[domain]=1
            else:
                self.domain_dps[domain]+=1

            if domain not in all_user_domains:
                all_user_domains[domain]=1
            else:
                all_user_domains[domain]+=1

        for domain in all_user_domains:
            if domain == "":
                continue
            if domain not in self.domain_uids:
                self.domain_uids[domain] = 1
            else:
                self.domain_uids[domain] += 1
        return

    def save_dict(self):
        d_u = self.domain_uids.items()
        d_u.sort(key=lambda x: -x[1])
        # with utf_open_write(self.fn+"_domain_uids.csv") as out_f:
        #     for el in d_u:
        #         out_f.write( el[0] + "\t" + str(el[1])+ "\r")
        #     pass
        # pass
        # d_u = self.domain_dps.items()
        # d_u.sort(key=lambda x: -x[1])
        # with utf_open_write(self.fn+"_domain_dps.csv") as out_f:
        #     for el in d_u:
        #         out_f.write( el[0] + "\t" + str(el[1])+ "\r")
        #     pass
        # pass

        for k in self.domain_dps:
            self.domain_uids_dps[k] = [self.domain_uids[k], self.domain_dps[k]]
        d_u = self.domain_uids_dps.items()
        d_u.sort(key=lambda x: -x[1][0])
        with utf_open_write(self.fn+"_domain_uids_dps.csv") as out_f:
            for el in d_u:
                out_f.write( el[0] + "\t" + str(el[1][0])+"\t" + str(el[1][1]) + "\r")
            pass
        pass


def process_toks(tokens_list, accs):
    if len(tokens_list)==0:
        return
    if len(accs)==0:
        return
    for acc in accs:
        acc.process_tokens(tokens_list)
    pass



def main():
    limit = None
    if len(sys.argv)<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден.", fn
        return
    pass

    accs = [DomainUIDsDPs(fn)]

    with utf_open_read(fn) as f:
        lc = 0
        uid = None
        accumulated_toks = []
        for line in f:
            lc+=1
            if lc%(100*1000) == 0:
                print "{} Processed {}k lines".format(ts(), lc/1000)
            if limit and (lc>limit):
                print "stopped on limit",lc, "lines"
                break

            tokens = line.replace("\r", "").replace("\n", "").strip().split("\t")
            if  len(tokens)==17:
                new_uid = tokens[0]
                if new_uid<>uid:
                    process_toks(accumulated_toks, accs)
                    accumulated_toks = []
                    uid = new_uid
                accumulated_toks.append(tokens)
        print "Saving dictionaries..."
        for acc in accs:
            acc.save_dict()

    print ts(), u"Обработка завершена. Окно будет закрыто через 5 минут"
    time.sleep(5*60)

    pass


if __name__ == '__main__':
    main()
    pass