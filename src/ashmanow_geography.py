# -*- coding: UTF-8 -*-
import codecs, time, collections


"""  Назначение модуля - очищать данные о географических признаков Ашманова """

""" кусок xml из спецификациии Ашманова, раздел "Мир" """


_world_xml = u"""
<Category ID="World" title="Мир">
    <Category ID="Russia" title="Россия">
      <Category ID="Povolzhje" title="Приволжье">
        <Category ID="Bashkiria" title="Башкортостан">
          <Category ID="Ufa" title="Уфа"/>
          <Category ID="Sterlitamak" title="Стерлитамак"/>
          <Category ID="Neftekamsk" title="Нефтекамск"/>
        </Category>
        <Category ID="Kirov" title="Кировская область">
          <Category ID="Kirov" title="Киров"/>
        </Category>
        <Category ID="MarijEl" title="Марий Эл">
          <Category ID="YoshkarOla" title="Йошкар-Ола"/>
        </Category>
        <Category ID="Mordovia" title="Мордовия">
          <Category ID="Saransk" title="Саранск"/>
        </Category>
        <Category ID="NizhnijNovgorod" title="Нижегородская область">
          <Category ID="NizhnijNovgorod" title="Нижний Новгород"/>
          <Category ID="Arzamas" title="Арзамас"/>
        </Category>
        <Category ID="Orenburg" title="Оренбургская область">
          <Category ID="Orenburg" title="Оренбург"/>
          <Category ID="Orsk" title="Орск"/>
        </Category>
        <Category ID="Penza" title="Пензенская область">
          <Category ID="Penza" title="Пенза"/>
        </Category>
        <Category ID="Perm" title="Пермский край">
          <Category ID="Perm" title="Пермь"/>
        </Category>
        <Category ID="Samara" title="Самарская область">
          <Category ID="Samara" title="Самара"/>
          <Category ID="Tolyatti" title="Тольятти"/>
          <Category ID="Balakovo" title="Балаково"/>
          <Category ID="Syzran" title="Сызрань"/>
          <Category ID="Novokuybyshevsk" title="Новокуйбышевск"/>
        </Category>
        <Category ID="Saratov" title="Саратовская область">
          <Category ID="Saratov" title="Саратов"/>
        </Category>
        <Category ID="Tataria" title="Татарстан">
          <Category ID="Kazan" title="Казань"/>
          <Category ID="NaberezhnyeChelny" title="Набережные Челны"/>
          <Category ID="Nizhnekamsk" title="Нижнекамск"/>
          <Category ID="Almetyevsk" title="Альметьевск"/>
        </Category>
        <Category ID="Udmurtia" title="Удмуртия">
          <Category ID="Izhevsk" title="Ижевск"/>
        </Category>
        <Category ID="Uljanovsk" title="Ульяновская область">
          <Category ID="Uljanovsk" title="Ульяновск"/>
          <Category ID="Dimitrovgrad" title="Димитровград"/>
        </Category>
        <Category ID="Chuvashia" title="Чувашия">
          <Category ID="Cheboksary" title="Чебоксары"/>
          <Category ID="Novocheboksarsk" title="Новочебоксарск"/>
        </Category>
      </Category>
      <Category ID="SouthOfRussia" title="Юг России">
        <Category ID="Adygeja" title="Адыгея">
          <Category ID="Maykop" title="Майкоп"/>
        </Category>
        <Category ID="Astrakhan" title="Астраханская область">
          <Category ID="Astrakhan" title="Астрахань"/>
        </Category>
        <Category ID="Volgograd" title="Волгоградская область">
          <Category ID="Volgograd" title="Волгоград"/>
          <Category ID="Volzhsky" title="Волжский"/>
          <Category ID="Kamyshin" title="Камышин"/>
        </Category>
        <Category ID="Kalmykia" title="Калмыкия">
          <Category ID="Elista" title="Элиста"/>
        </Category>
        <Category ID="Krasnodar" title="Краснодарский край">
          <Category ID="Krasnodar" title="Краснодар"/>
          <Category ID="Sochi" title="Сочи"/>
          <Category ID="Novorossiysk" title="Новороссийск"/>
        </Category>
        <Category ID="Rostov" title="Ростовская область">
          <Category ID="RostovOnDon" title="Ростов-на-Дону"/>
          <Category ID="Taganrog" title="Таганрог"/>
          <Category ID="Novocherkassk" title="Новочеркасск"/>
          <Category ID="Volgodonsk" title="Волгодонск"/>
          <Category ID="Novoshakhtinsk" title="Новошахтинск"/>
          <Category ID="Bataysk" title="Батайск"/>
        </Category>
        <Category ID="NorthCaucasus" title="Северный Кавказ">
          <Category ID="Dagestan" title="Дагестан">
            <Category ID="Makhachkala" title="Махачкала"/>
            <Category ID="Khasavyurt" title="Хасавюрт"/>
            <Category ID="Derbent" title="Дербент"/>
          </Category>
          <Category ID="Ingushetia" title="Ингушетия">
            <Category ID="Magas" title="Магас"/>
            <Category ID="Nazran" title="Назрань"/>
          </Category>
          <Category ID="KabardinoBalkaria" title="Кабардино-Балкария">
            <Category ID="Nalchik" title="Нальчик"/>
          </Category>
          <Category ID="KarachajevoCherkesia" title="Карачаево-Черкесия">
            <Category ID="Cherkessk" title="Черкесск"/>
          </Category>
          <Category ID="NorthOsetia" title="Северная Осетия">
            <Category ID="Vladikavkaz" title="Владикавказ"/>
          </Category>
          <Category ID="Stavropolje" title="Ставропольский край">
            <Category ID="Stavropol" title="Ставрополь"/>
            <Category ID="Pyatigorsk" title="Пятигорск"/>
            <Category ID="Kislovodsk" title="Кисловодск"/>
            <Category ID="Nevinnomyssk" title="Невинномысск"/>
          </Category>
          <Category ID="Chechnya" title="Чечня">
            <Category ID="Grozny" title="Грозный"/>
          </Category>
        </Category>
      </Category>
      <Category ID="Crimea" title="Крым">
        <Category ID="Sevastopol" title="Севастополь"/>
      </Category>
      <Category ID="Urals" title="Урал">
        <Category ID="Kurgan" title="Курганская область">
          <Category ID="Kurgan" title="Курган"/>
        </Category>
        <Category ID="Sverdlovsk" title="Свердловская область">
          <Category ID="Yekaterinburg" title="Екатеринбург"/>
          <Category ID="NizhnyTagil" title="Нижний Тагил"/>
          <Category ID="KamenskUralsky" title="Каменск-Уральский"/>
          <Category ID="Pervouralsk" title="Первоуральск"/>
        </Category>
        <Category ID="Tyumen" title="Тюменская область">
          <Category ID="Tyumen" title="Тюмень"/>
          <Category ID="Tobolsk" title="Тобольск"/>
        </Category>
        <Category ID="KhantyMansijskijAO" title="Ханты-Мансийский АО">
          <Category ID="Surgut" title="Сургут"/>
          <Category ID="Nizhnevartovsk" title="Нижневартовск"/>
          <Category ID="Nefteyugansk" title="Нефтеюганск"/>
        </Category>
        <Category ID="YamaloNenetskijAO" title="Ямало-Ненецкий АО">
          <Category ID="NovyUrengoy" title="Новый Уренгой"/>
          <Category ID="Noyabrsk" title="Ноябрьск"/>
        </Category>
        <Category ID="Chelyabinsk" title="Челябинская область">
          <Category ID="Chelyabinsk" title="Челябинск"/>
          <Category ID="Magnitogorsk" title="Магнитогорск"/>
          <Category ID="Zlatoust" title="Златоуст"/>
          <Category ID="Miass" title="Миасс"/>
          <Category ID="Kopeysk" title="Копейск"/>
        </Category>
      </Category>
      <Category ID="Siberia" title="Сибирь">
        <Category ID="AltaiTerritory" title="Алтайский край">
          <Category ID="Barnaul" title="Барнаул"/>
          <Category ID="Biysk" title="Бийск"/>
          <Category ID="Rubtsovsk" title="Рубцовск"/>
        </Category>
        <Category ID="Buryatia" title="Бурятия">
          <Category ID="UlanUde" title="Улан-Удэ"/>
        </Category>
        <Category ID="Irkutsk" title="Иркутская область">
          <Category ID="Irkutsk" title="Иркутск"/>
          <Category ID="Bratsk" title="Братск"/>
          <Category ID="Angarsk" title="Ангарск"/>
          <Category ID="UstOrdyn" title="Усть-Ордынский Бурятский АО"/>
        </Category>
        <Category ID="Chita" title="Забайкальский край">
          <Category ID="Chita" title="Чита"/>
          <Category ID="AginsBurAO" title="Агинский Бурятский АО"/>
        </Category>
        <Category ID="Kemerovo" title="Кемеровская область">
          <Category ID="Kemerovo" title="Кемерово"/>
          <Category ID="Novokuznetsk" title="Новокузнецк"/>
          <Category ID="Prokopyevsk" title="Прокопьевск"/>
          <Category ID="LeninskKuznetsky" title="Ленинск-Кузнецкий"/>
          <Category ID="Mezhdurechensk" title="Междуреченск"/>
          <Category ID="Kiselevsk" title="Киселевск"/>
        </Category>
        <Category ID="Krasnoyarsk" title="Красноярский край">
          <Category ID="Krasnoyarsk" title="Красноярск"/>
          <Category ID="Norilsk" title="Норильск"/>
          <Category ID="Achinsk" title="Ачинск"/>
          <Category ID="Taimyr" title="Таймыр"/>
          <Category ID="Evenkia" title="Эвенкия"/>
        </Category>
        <Category ID="Novosibirsk" title="Новосибирская область">
          <Category ID="Novosibirsk" title="Новосибирск"/>
        </Category>
        <Category ID="Omsk" title="Омская область">
          <Category ID="Omsk" title="Омск"/>
        </Category>
        <Category ID="RepublicOfAltai" title="Республика Алтай">
          <Category ID="GornoAltaysk" title="Горно-Алтайск"/>
        </Category>
        <Category ID="Tomsk" title="Томская область">
          <Category ID="Tomsk" title="Томск"/>
        </Category>
        <Category ID="Tuva" title="Тыва">
          <Category ID="Kyzyl" title="Кызыл"/>
        </Category>
        <Category ID="Hakassia" title="Хакасия">
          <Category ID="Abakan" title="Абакан"/>
        </Category>
      </Category>
      <Category ID="FarEast" title="Дальний Восток">
        <Category ID="Amur" title="Амурская область"/>
        <Category ID="Birobidzhan" title="Еврейская автономная область">
          <Category ID="Birobidzhan" title="Биробиджан"/>
        </Category>
        <Category ID="Kamchatka" title="Камчатский край">
          <Category ID="PetropavlovskKamchatsky" title="Петропавловск-Камчатский"/>
          <Category ID="Koryakia" title="Корякия"/>
        </Category>
        <Category ID="Magadan" title="Магаданская область">
          <Category ID="Magadan" title="Магадан"/>
        </Category>
        <Category ID="Primorje" title="Приморский край">
          <Category ID="Vladivostok" title="Владивосток"/>
          <Category ID="Ussuriysk" title="Уссурийск"/>
        </Category>
        <Category ID="Sakhalin" title="Сахалинская область">
          <Category ID="YuzhnoSakhalinsk" title="Южно-Сахалинск"/>
        </Category>
        <Category ID="Chukotka" title="Чукотский АО">
          <Category ID="Anadyr" title="Анадырь"/>
        </Category>
        <Category ID="Khabarovsk" title="Хабаровский край">
          <Category ID="Khabarovsk" title="Хабаровск"/>
          <Category ID="KomsomolskOnAmur" title="Комсомольск-на-Амуре"/>
        </Category>
        <Category ID="Yakutia" title="Якутия">
          <Category ID="Yakutsk" title="Якутск"/>
        </Category>
      </Category>
      <Category ID="MosObl" title="Москва и область">
        <Category ID="Moscow" title="Москва">
          <Category ID="CentralDistrict" title="Центральный округ">
            <Category ID="Arbat" title="Арбат"/>
            <Category ID="Basmanny" title="Басманный район"/>
            <Category ID="Zamoskvorechye" title="Замоскворечье"/>
            <Category ID="Krasnoselsky" title="Красносельский район"/>
            <Category ID="Meshchansky" title="Мещанский район"/>
            <Category ID="Presnensky" title="Пресненский район"/>
            <Category ID="Tagansky" title="Таганский район"/>
            <Category ID="Tverskoy" title="Тверской район"/>
            <Category ID="Khamovniki" title="Хамовники"/>
            <Category ID="Yakimanka" title="Якиманка"/>
          </Category>
          <Category ID="NorthDistrict" title="Северный округ">
            <Category ID="Aeroport" title="Аэропорт"/>
            <Category ID="Begovoy" title="Беговой"/>
            <Category ID="Beskudnikovsky" title="Бескудниковский район"/>
            <Category ID="Voykovsky" title="Войковский район"/>
            <Category ID="VostochnoeDegunino" title="Восточное Дегунино"/>
            <Category ID="Golovinsky" title="Головинский район"/>
            <Category ID="Dmitrovsky" title="Дмитровский район"/>
            <Category ID="ZapadnoeDegunino" title="Западное Дегунино"/>
            <Category ID="Koptevo" title="Коптево"/>
            <Category ID="Levoberezhny" title="Левобережный"/>
            <Category ID="Molzhaninovsky" title="Молжаниновский район"/>
            <Category ID="Savyolovsky" title="Савеловский район"/>
            <Category ID="Sokol" title="Сокол"/>
            <Category ID="Timiryazevsky" title="Тимирязевский район"/>
            <Category ID="Khovrino" title="Ховрино"/>
            <Category ID="Khoroshyovsky" title="Хорошевский район"/>
          </Category>
          <Category ID="NorthEastDistrict" title="Северо-Восточный округ">
            <Category ID="Alexeevsky" title="Алексеевский район"/>
            <Category ID="Altufyevsky" title="Алтуфьевский район"/>
            <Category ID="Babushkinsky" title="Бабушкинский район"/>
            <Category ID="Bibirevo" title="Бибирево"/>
            <Category ID="Butyrsky" title="Бутырский район"/>
            <Category ID="Lianozovo" title="Лианозово"/>
            <Category ID="Losinoostrovsky" title="Лосиноостровский район"/>
            <Category ID="Marfino" title="Марфино"/>
            <Category ID="MaryinaRoshcha" title="Марьина роща"/>
            <Category ID="Ostankinsky" title="Останкинский район"/>
            <Category ID="Otradnoe" title="Отрадное"/>
            <Category ID="Rostokino" title="Ростокино"/>
            <Category ID="Sviblovo" title="Свиблово"/>
            <Category ID="SevernoeMedvedkovo" title="Северное Медведково"/>
            <Category ID="Severny" title="Северный"/>
            <Category ID="YuzhnoeMedvedkovo" title="Южное Медведково"/>
            <Category ID="Yaroslavsky" title="Ярославский район"/>
          </Category>
          <Category ID="EastDistrict" title="Восточный округ">
            <Category ID="Bogorodskoe" title="Богородское"/>
            <Category ID="Veshnyaki" title="Вешняки"/>
            <Category ID="VostochnoeIzmaylovo" title="Восточное Измайлово"/>
            <Category ID="Vostochny" title="Восточный"/>
            <Category ID="Golyanovo" title="Гольяново"/>
            <Category ID="Ivanovskoe" title="Ивановское"/>
            <Category ID="Izmaylovo" title="Измайлово"/>
            <Category ID="KosinoUkhtomsky" title="Косино-Ухтомский"/>
            <Category ID="Metrogorodok" title="Метрогородок"/>
            <Category ID="Novogireevo" title="Новогиреево"/>
            <Category ID="Novokosino" title="Новокосино"/>
            <Category ID="Perovo" title="Перово"/>
            <Category ID="Preobrazhenskoe" title="Преображенское"/>
            <Category ID="SevernoeIzmaylovo" title="Северное Измайлово"/>
            <Category ID="SokolinayaGora" title="Соколиная гора"/>
            <Category ID="Sokolniki" title="Сокольники"/>
          </Category>
          <Category ID="SouthEastDistrict" title="Юго-Восточный округ">
            <Category ID="VykhinoZhulebino" title="Выхино-Жулебино"/>
            <Category ID="Kapotnya" title="Капотня"/>
            <Category ID="Kuzminki" title="Кузьминки"/>
            <Category ID="Lefortovo" title="Лефортово"/>
            <Category ID="Lyublino" title="Люблино"/>
            <Category ID="Maryino" title="Марьино"/>
            <Category ID="Nekrasovka" title="Некрасовка"/>
            <Category ID="Nizhegorodsky" title="Нижегородский район"/>
            <Category ID="Pechatniki" title="Печатники"/>
            <Category ID="Ryazansky" title="Рязанский район"/>
            <Category ID="Tekstilshchiki" title="Текстильщики"/>
            <Category ID="Yuzhnoportovy" title="Южнопортовый район"/>
          </Category>
          <Category ID="SouthDistrict" title="Южный округ">
            <Category ID="BiryulyovoVostochnoe" title="Бирюлево Восточное"/>
            <Category ID="BiryulyovoZapadnoe" title="Бирюлево Западное"/>
            <Category ID="Brateevo" title="Братеево"/>
            <Category ID="Danilovsky" title="Даниловский район"/>
            <Category ID="Donskoy" title="Донской район"/>
            <Category ID="Zyablikovo" title="Зябликово"/>
            <Category ID="MoskvorechyeSaburovo" title="Москворечье-Сабурово"/>
            <Category ID="NagatinoSadovniki" title="Нагатино-Садовники"/>
            <Category ID="NagatinskyZaton" title="Нагатинский затон"/>
            <Category ID="Nagorny" title="Нагорный район"/>
            <Category ID="OrekhovoBorisovoSevernoe" title="Орехово-Борисово Северное"/>
            <Category ID="OrekhovoBorisovoYuzhnoe" title="Орехово-Борисово Южное"/>
            <Category ID="Tsaritsyno" title="Царицыно"/>
            <Category ID="ChertanovoSevernoe" title="Чертаново Северное"/>
            <Category ID="ChertanovoTsentralnoe" title="Чертаново Центральное"/>
            <Category ID="ChertanovoYuzhnoe" title="Чертаново Южное"/>
          </Category>
          <Category ID="SouthWestDistrict" title="Юго-Западный округ">
            <Category ID="Akademichesky" title="Академический район"/>
            <Category ID="Gagarinsky" title="Гагаринский район"/>
            <Category ID="Zyuzino" title="Зюзино"/>
            <Category ID="Konkovo" title="Коньково"/>
            <Category ID="Kotlovka" title="Котловка"/>
            <Category ID="Lomonosovsky" title="Ломоносовский район"/>
            <Category ID="Obruchevsky" title="Обручевский район"/>
            <Category ID="SevernoeButovo" title="Северное Бутово"/>
            <Category ID="TyoplyStan" title="Теплый Стан"/>
            <Category ID="Cheryomushki" title="Черемушки"/>
            <Category ID="YuzhnoeButovo" title="Южное Бутово"/>
            <Category ID="Yasenevo" title="Ясенево"/>
          </Category>
          <Category ID="WestDistrict" title="Западный округ">
            <Category ID="Vnukovo" title="Внуково"/>
            <Category ID="Dorogomilovo" title="Дорогомилово"/>
            <Category ID="Krylatskoe" title="Крылатское"/>
            <Category ID="Kuntsevo" title="Кунцево"/>
            <Category ID="Mozhaysky" title="Можайский район"/>
            <Category ID="NovoPeredelkino" title="Ново-Переделкино"/>
            <Category ID="OchakovoMatveevskoe" title="Очаково-Матвеевское"/>
            <Category ID="ProspektVernadskogo" title="Проспект Вернадского"/>
            <Category ID="Ramenki" title="Раменки"/>
            <Category ID="Solntsevo" title="Солнцево"/>
            <Category ID="TroparyovoNikulino" title="Тропарево-Никулино"/>
            <Category ID="FilyovskyPark" title="Филевский парк"/>
            <Category ID="FiliDavydkovo" title="Фили-Давыдково"/>
          </Category>
          <Category ID="NorthWestDistrict" title="Северо-Западный округ">
            <Category ID="Kurkino" title="Куркино"/>
            <Category ID="Mitino" title="Митино"/>
            <Category ID="PokrovskoeStreshevo" title="Покровское-Стрешнево"/>
            <Category ID="SevernoeTushino" title="Северное Тушино"/>
            <Category ID="Strogino" title="Строгино"/>
            <Category ID="KhoroshyovoMnyovniki" title="Хорошево-Мневники"/>
            <Category ID="Shchukino" title="Щукино"/>
            <Category ID="YuzhnoeTushino" title="Южное Тушино"/>
          </Category>
          <Category ID="ZelenogradDistrict" title="Зеленоградский округ">
            <Category ID="Kryukovo" title="Крюково"/>
            <Category ID="Matushkino" title="Матушкино"/>
            <Category ID="Savyolki" title="Савелки"/>
            <Category ID="Silino" title="Силино"/>
            <Category ID="StaroeKryukovo" title="Старое Крюково"/>
          </Category>
          <Category ID="NovomoskovskyDistrict" title="Новомосковский округ"/>
          <Category ID="TroitskyDistrict" title="Троицкий округ"/>
        </Category>
        <Category ID="MoscowRegion" title="Московская область">
          <Category ID="Balashikha" title="Балашиха"/>
          <Category ID="Kolomna" title="Коломна"/>
          <Category ID="Lyubertsy" title="Люберцы"/>
          <Category ID="Mytishchi" title="Мытищи"/>
          <Category ID="Noginsk" title="Ногинск"/>
          <Category ID="Odintsovo" title="Одинцово"/>
          <Category ID="OrekhovoZuyevo" title="Орехово-Зуево"/>
          <Category ID="Podolsk" title="Подольск"/>
          <Category ID="SergievPosad" title="Сергиев Посад"/>
          <Category ID="Serpukhov" title="Серпухов"/>
          <Category ID="Khimki" title="Химки"/>
          <Category ID="Shchyolkovo" title="Щелково"/>
          <Category ID="Electrostal" title="Электросталь"/>
        </Category>
      </Category>
      <Category ID="CentralRussia" title="Центральная Россия">
        <Category ID="Belgorod" title="Белгородская область">
          <Category ID="Belgorod" title="Белгород"/>
          <Category ID="StaryOskol" title="Старый Оскол"/>
        </Category>
        <Category ID="Bryansk" title="Брянская область">
          <Category ID="Bryansk" title="Брянск"/>
        </Category>
        <Category ID="Vladimir" title="Владимирская область">
          <Category ID="Vladimir" title="Владимир"/>
          <Category ID="Murom" title="Муром"/>
        </Category>
        <Category ID="Voronezh" title="Воронежская область">
          <Category ID="Voronezh" title="Воронеж"/>
        </Category>
        <Category ID="Ivanovo" title="Ивановская область">
          <Category ID="Ivanovo" title="Иваново"/>
        </Category>
        <Category ID="Kaluga" title="Калужская область">
          <Category ID="Kaluga" title="Калуга"/>
          <Category ID="Obninsk" title="Обнинск"/>
        </Category>
        <Category ID="Kostroma" title="Костромская область">
          <Category ID="Kostroma" title="Кострома"/>
        </Category>
        <Category ID="Kursk" title="Курская область">
          <Category ID="Kursk" title="Курск"/>
        </Category>
        <Category ID="Lipetsk" title="Липецкая область">
          <Category ID="Lipetsk" title="Липецк"/>
          <Category ID="Yelets" title="Елец"/>
        </Category>
        <Category ID="Orel" title="Орловская область">
          <Category ID="Orel" title="Орел"/>
        </Category>
        <Category ID="Ryazan" title="Рязанская область">
          <Category ID="Ryazan" title="Рязань"/>
        </Category>
        <Category ID="Smolensk" title="Смоленская область">
          <Category ID="Smolensk" title="Смоленск"/>
        </Category>
        <Category ID="Tambov" title="Тамбовская область">
          <Category ID="Tambov" title="Тамбов"/>
        </Category>
        <Category ID="Tver" title="Тверская область">
          <Category ID="Tver" title="Тверь"/>
        </Category>
        <Category ID="Tula" title="Тульская область">
          <Category ID="Tula" title="Тула"/>
        </Category>
        <Category ID="Yaroslavl" title="Ярославская область">
          <Category ID="Yaroslavl" title="Ярославль"/>
          <Category ID="Rybinsk" title="Рыбинск"/>
        </Category>
      </Category>
      <Category ID="NorthWest" title="Северо-Запад">
        <Category ID="SpbObl" title="Санкт-Петербург и область">
          <Category ID="SaintPetersburg" title="Санкт-Петербург">
            <Category ID="AdmiralteyskyDistrict" title="Адмиралтейский район"/>
            <Category ID="VasileostrovskyDistrict" title="Василеостровский район"/>
            <Category ID="VyborgskyDistrict" title="Выборгский район"/>
            <Category ID="KalininskyDistrict" title="Калининский район"/>
            <Category ID="KirovskyDistrict" title="Кировский район"/>
            <Category ID="KolpinskyDistrict" title="Колпинский район"/>
            <Category ID="KrasnogvardeyskyDistrict" title="Красногвардейский район"/>
            <Category ID="KrasnoselskyDistrict" title="Красносельский район"/>
            <Category ID="KronshtadtskyDistrict" title="Кронштадтский район"/>
            <Category ID="KurortnyDistrict" title="Курортный район"/>
            <Category ID="MoskovskyDistrict" title="Московский район"/>
            <Category ID="NevskyDistrict" title="Невский район"/>
            <Category ID="PetrogradskyDistrict" title="Петроградский район"/>
            <Category ID="PetrodvortsovyDistrict" title="Петродворцовый район"/>
            <Category ID="PrimorskyDistrict" title="Приморский район"/>
            <Category ID="PushkinskyDistrict" title="Пушкинский район"/>
            <Category ID="FrunzenskyDistrict" title="Фрунзенский район"/>
            <Category ID="TsentralnyDistrict" title="Центральный район"/>
          </Category>
          <Category ID="Leningrad" title="Ленинградская область"/>
        </Category>
        <Category ID="Arkhangelsk" title="Архангельская область">
          <Category ID="Arkhangelsk" title="Архангельск"/>
          <Category ID="Severodvinsk" title="Северодвинск"/>
          <Category ID="NenetskyAO" title="Ненецкий АО"/>
        </Category>
        <Category ID="Vologda" title="Вологодская область">
          <Category ID="Vologda" title="Вологда"/>
          <Category ID="Cherepovets" title="Череповец"/>
        </Category>
        <Category ID="Kaliningrad" title="Калининградская область">
          <Category ID="Kaliningrad" title="Калининград"/>
        </Category>
        <Category ID="Karelia" title="Карелия">
          <Category ID="Petrozavodsk" title="Петрозаводск"/>
        </Category>
        <Category ID="Komi" title="Коми">
          <Category ID="Syktyvkar" title="Сыктывкар"/>
          <Category ID="Ukhta" title="Ухта"/>
        </Category>
        <Category ID="Murmansk" title="Мурманская область">
          <Category ID="Murmansk" title="Мурманск"/>
        </Category>
        <Category ID="Novgorod" title="Новгородская область">
          <Category ID="Novgorod" title="Великий Новгород"/>
        </Category>
        <Category ID="Pskov" title="Псковская область">
          <Category ID="Pskov" title="Псков"/>
          <Category ID="VelikieLuki" title="Великие Луки"/>
        </Category>
      </Category>
    </Category>
    <Category ID="Asia" title="Азия">
      <Category ID="SouthEasternAsia" title="Юго-Восточная Азия">
        <Category ID="Vietnam" title="Вьетнам">
          <Category ID="NorthernVietnam" title="Северный Вьетнам">
            <Category ID="RedRiverDelta" title="Дельта Красной реки">
              <Category ID="HaNoi" title="Ханой">
                <Category ID="BaDinh" title="Бадинь"/>
                <Category ID="DongDa" title="Донгда"/>
                <Category ID="CauGiay" title="Каузяй"/>
                <Category ID="LongBien" title="Лонгбьен"/>
                <Category ID="TayHo" title="Тайхо"/>
                <Category ID="ThanhXuan" title="Тханьсуан"/>
                <Category ID="HaiBaTrung" title="Хайбачынг"/>
                <Category ID="HoanKiem" title="Хоанкьем"/>
                <Category ID="DongAnh" title="Dong Anh"/>
                <Category ID="GiaLam" title="Gia Lam"/>
                <Category ID="ThanhTri" title="Thanh Tri"/>
                <Category ID="TuLiem" title="Tu Liem"/>
                <Category ID="SocSon" title="Soc Son"/>
              </Category>
              <Category ID="HaiPhong" title="Хайфон"/>
              <Category ID="BacNinh" title="Бакнинь"/>
              <Category ID="VinhPhuc" title="Виньфук"/>
              <Category ID="NamDinh" title="Намдинь"/>
              <Category ID="NinhBinh" title="Ниньбинь"/>
              <Category ID="ThaiBinh" title="Тхайбинь"/>
              <Category ID="HaiDuong" title="Хайзыонг"/>
              <Category ID="HaNam" title="Ханам"/>
              <Category ID="HungYen" title="Хынгйен"/>
            </Category>
            <Category ID="Northeast" title="Северо-Восточный регион">
              <Category ID="BacGiang" title="Бакзянг"/>
              <Category ID="BacKan" title="Баккан"/>
              <Category ID="YenBai" title="Йенбай"/>
              <Category ID="CaoBang" title="Каобанг"/>
              <Category ID="QuangNinh" title="Куангнинь"/>
              <Category ID="LangSon" title="Лангшон"/>
              <Category ID="LaoCai" title="Лаокай"/>
              <Category ID="TuyenQuang" title="Туенкуанг"/>
              <Category ID="ThaiNguyen" title="Тхайнгуен"/>
              <Category ID="PhuTho" title="Футхо"/>
              <Category ID="HaGiang" title="Хазянг"/>
            </Category>
            <Category ID="Northwest" title="Северо-Западный регион">
              <Category ID="DienBien" title="Дьенбьен"/>
              <Category ID="LaiChau" title="Лайтяу"/>
              <Category ID="HoaBinh" title="Хоабинь"/>
              <Category ID="SonLa" title="Шонла"/>
            </Category>
          </Category>
          <Category ID="CentralVietnam" title="Центральный Вьетнам">
            <Category ID="NorthCentralCoast" title="Север Центрального побережья">
              <Category ID="QuangBinh" title="Куангбинь"/>
              <Category ID="QuangTri" title="Куангчи"/>
              <Category ID="NgheAn" title="Нгеан"/>
              <Category ID="ThanhHoa" title="Тханьхоа"/>
              <Category ID="ThuaThienHue" title="Тхыатхьен-Хюэ"/>
              <Category ID="HaTinh" title="Хатинь"/>
            </Category>
            <Category ID="SouthCentralCoast" title="Юг Центрального побережья">
              <Category ID="DaNang" title="Дананг"/>
              <Category ID="BinhDinh" title="Биньдинь"/>
              <Category ID="BinhThuan" title="Биньтхуан"/>
              <Category ID="QuangNam" title="Куангнам"/>
              <Category ID="QuangNgai" title="Куангнгай"/>
              <Category ID="KhanhHoa" title="Кханьхоа"/>
              <Category ID="NinhThuan" title="Ниньтхуан"/>
              <Category ID="PhuYen" title="Фуйен"/>
            </Category>
            <Category ID="CentralHighlands" title="Центральное нагорье">
              <Category ID="DakLak" title="Даклак"/>
              <Category ID="DakNong" title="Дакнонг"/>
              <Category ID="GiaLai" title="Зялай"/>
              <Category ID="KonTum" title="Контум"/>
              <Category ID="LamDong" title="Ламдонг"/>
            </Category>
          </Category>
          <Category ID="SouthernVietnam" title="Южный Вьетнам">
            <Category ID="Southeast" title="Юго-Восточный регион">
              <Category ID="HoChiMinh" title="Хошимин"/>
              <Category ID="BaRiaVungTau" title="Бариа-Вунгтау"/>
              <Category ID="BinhDuong" title="Биньзыонг"/>
              <Category ID="BinhPhuoc" title="Биньфыок"/>
              <Category ID="DongNai" title="Донгнай"/>
              <Category ID="TayNinh" title="Тэйнинь"/>
            </Category>
            <Category ID="MekongRiverDelta" title="Дельта Меконга">
              <Category ID="CanTho" title="Кантхо"/>
              <Category ID="AnGiang" title="Анзянг"/>
              <Category ID="BacLieu" title="Бакльеу"/>
              <Category ID="BenTre" title="Бенче"/>
              <Category ID="VinhLong" title="Виньлонг"/>
              <Category ID="DongThap" title="Донгтхап"/>
              <Category ID="CaMau" title="Камау"/>
              <Category ID="KienGiang" title="Кьензянг"/>
              <Category ID="LongAn" title="Лонган"/>
              <Category ID="TienGiang" title="Тьензянг"/>
              <Category ID="HauGiang" title="Хаузянг"/>
              <Category ID="TraVinh" title="Чавинь"/>
              <Category ID="SocTrang" title="Шокчанг"/>
            </Category>
          </Category>
        </Category>
        <Category ID="Brunei" title="Бруней"/>
        <Category ID="EastTimor" title="Восточный Тимор"/>
        <Category ID="Indonesia" title="Индонезия"/>
        <Category ID="Cambodia" title="Камбоджа"/>
        <Category ID="Laos" title="Лаос"/>
        <Category ID="Malaysia" title="Малайзия"/>
        <Category ID="Myanmar" title="Мьянма"/>
        <Category ID="Singapore" title="Сингапур"/>
        <Category ID="Thailand" title="Таиланд"/>
        <Category ID="Philippines" title="Филиппины"/>
      </Category>
      <Category ID="EasternAsia" title="Восточная Азия">
        <Category ID="China" title="Китай">
          <Category ID="HongKong" title="Гонконг"/>
          <Category ID="Macau" title="Макао"/>
          <Category ID="Taiwan" title="Тайвань"/>
        </Category>
        <Category ID="Korea" title="Корея">
          <Category ID="NorthKorea" title="КНДР"/>
          <Category ID="SouthKorea" title="Южная Корея"/>
        </Category>
        <Category ID="Mongolia" title="Монголия"/>
        <Category ID="Japan" title="Япония"/>
      </Category>
      <Category ID="SouthernAsia" title="Южная Азия">
        <Category ID="Bangladesh" title="Бангладеш"/>
        <Category ID="Bhutan" title="Бутан"/>
        <Category ID="India" title="Индия"/>
        <Category ID="Maldives" title="Мальдивы"/>
        <Category ID="Nepal" title="Непал"/>
        <Category ID="Pakistan" title="Пакистан"/>
        <Category ID="SriLanka" title="Шри-Ланка"/>
      </Category>
      <Category ID="CentralAsia" title="Центральная Азия">
        <Category ID="Kazakhstan" title="Казахстан">
          <Category ID="Astana" title="Астана"/>
          <Category ID="Baykonur" title="Байконур"/>
          <Category ID="AlmaAtaObl" title="Алматинская область">
            <Category ID="AlmaAta" title="Алма-Ата"/>
          </Category>
          <Category ID="Akmolinsk" title="Акмолинская область"/>
          <Category ID="Aktyubinsk" title="Актюбинская область"/>
          <Category ID="Atyrau" title="Атырауская область"/>
          <Category ID="EastKazakh" title="Восточно-Казахстанская область"/>
          <Category ID="Zhambyl" title="Жамбылская область"/>
          <Category ID="WestKazakh" title="Западно-Казахстанская область"/>
          <Category ID="Karaganda" title="Карагандинская область"/>
          <Category ID="Kostanay" title="Костанайская область"/>
          <Category ID="KzylOrda" title="Кызылординская область"/>
          <Category ID="Mangistau" title="Мангистауская область"/>
          <Category ID="Pavlodar" title="Павлодарская область"/>
          <Category ID="NorthKazakh" title="Северо-Казахстанская область"/>
          <Category ID="SouthKazakh" title="Южно-Казахстанская область"/>
        </Category>
        <Category ID="Kirgizia" title="Киргизия"/>
        <Category ID="Tajikistan" title="Таджикистан"/>
        <Category ID="Turkmenia" title="Туркмения"/>
        <Category ID="Uzbekistan" title="Узбекистан"/>
      </Category>
      <Category ID="WesternAsia" title="Западная Азия">
        <Category ID="Afghanistan" title="Афганистан"/>
        <Category ID="Bahrain" title="Бахрейн"/>
        <Category ID="Israel" title="Израиль"/>
        <Category ID="Palestine" title="Палестина"/>
        <Category ID="Jordan" title="Иордания">
          <Category ID="Amman" title="Амман"/>
        </Category>
        <Category ID="Yemen" title="Йемен">
          <Category ID="Sanaa" title="Сана"/>
        </Category>
        <Category ID="Iraq" title="Ирак"/>
        <Category ID="Iran" title="Иран"/>
        <Category ID="Qatar" title="Катар">
          <Category ID="Doha" title="Доха"/>
        </Category>
        <Category ID="Kuwait" title="Кувейт"/>
        <Category ID="Lebanon" title="Ливан">
          <Category ID="Beirut" title="Бейрут"/>
        </Category>
        <Category ID="ArabEmirates" title="ОАЭ">
          <Category ID="AbuDhabi" title="Абу-Даби"/>
          <Category ID="Ajman" title="Аджман"/>
          <Category ID="Dubai" title="Дубай"/>
          <Category ID="RasAlKhaimah" title="Рас-эль-Хайма"/>
          <Category ID="UmmAlQuwain" title="Умм-эль-Кайвайн"/>
          <Category ID="Fujairah" title="Фуджейра"/>
          <Category ID="Sharjah" title="Шарджа"/>
        </Category>
        <Category ID="Oman" title="Оман">
          <Category ID="Muscat" title="Маскат"/>
          <Category ID="Dhofar" title="Дофар"/>
        </Category>
        <Category ID="SaudiArabia" title="Саудовская Аравия">
          <Category ID="ArRiyad" title="Эр-Рияд"/>
          <Category ID="AshSharqiyah" title="Восточная провинция"/>
          <Category ID="Makkah" title="Мекка"/>
          <Category ID="AlMadinah" title="Эль-Мадина"/>
        </Category>
        <Category ID="Syria" title="Сирия">
          <Category ID="Damascus" title="Дамаск"/>
          <Category ID="Latakia" title="Латакия"/>
          <Category ID="Aleppo" title="Халеб"/>
        </Category>
      </Category>
    </Category>
    <Category ID="America" title="Америка">
      <Category ID="NorthAmerica" title="Северная Америка">
        <Category ID="USA" title="США">
          <Category ID="Idaho" title="Айдахо"/>
          <Category ID="Iowa" title="Айова"/>
          <Category ID="Alabama" title="Алабама"/>
          <Category ID="Alaska" title="Аляска"/>
          <Category ID="Arizona" title="Аризона"/>
          <Category ID="Arkansas" title="Арканзас"/>
          <Category ID="Wyoming" title="Вайоминг"/>
          <Category ID="Washington" title="Вашингтон"/>
          <Category ID="DistrictOfColumbia" title="Вашингтон, округ Колумбия"/>
          <Category ID="Vermont" title="Вермонт"/>
          <Category ID="Virginia" title="Вирджиния"/>
          <Category ID="Wisconsin" title="Висконсин"/>
          <Category ID="Hawaii" title="Гавайи"/>
          <Category ID="Delaware" title="Делавэр"/>
          <Category ID="Georgia" title="Джорджия"/>
          <Category ID="WestVirginia" title="Западная Вирджиния"/>
          <Category ID="Illinois" title="Иллинойс"/>
          <Category ID="Indiana" title="Индиана"/>
          <Category ID="California" title="Калифорния"/>
          <Category ID="Kansas" title="Канзас"/>
          <Category ID="Kentucky" title="Кентукки"/>
          <Category ID="Colorado" title="Колорадо"/>
          <Category ID="Connecticut" title="Коннектикут"/>
          <Category ID="Louisiana" title="Луизиана"/>
          <Category ID="Massachusetts" title="Массачусетс"/>
          <Category ID="Minnesota" title="Миннесота"/>
          <Category ID="Mississippi" title="Миссисипи"/>
          <Category ID="Missouri" title="Миссури"/>
          <Category ID="Michigan" title="Мичиган"/>
          <Category ID="Montana" title="Монтана"/>
          <Category ID="Maine" title="Мэн"/>
          <Category ID="Maryland" title="Мэриленд"/>
          <Category ID="Nebraska" title="Небраска"/>
          <Category ID="Nevada" title="Невада"/>
          <Category ID="NewJersey" title="Нью-Джерси"/>
          <Category ID="NewYork" title="Нью-Йорк"/>
          <Category ID="NewMexico" title="Нью-Мексико"/>
          <Category ID="NewHampshire" title="Нью-Хэмпшир"/>
          <Category ID="Ohio" title="Огайо"/>
          <Category ID="Oklahoma" title="Оклахома"/>
          <Category ID="Oregon" title="Орегон"/>
          <Category ID="Pennsylvania" title="Пенсильвания"/>
          <Category ID="RhodeIsland" title="Род-Айленд"/>
          <Category ID="NorthDakota" title="Северная Дакота"/>
          <Category ID="NorthCarolina" title="Северная Каролина"/>
          <Category ID="Tennessee" title="Теннесси"/>
          <Category ID="Texas" title="Техас"/>
          <Category ID="Florida" title="Флорида"/>
          <Category ID="SouthDakota" title="Южная Дакота"/>
          <Category ID="SouthCarolina" title="Южная Каролина"/>
          <Category ID="Utah" title="Юта"/>
        </Category>
        <Category ID="Canada" title="Канада"/>
        <Category ID="Mexico" title="Мексика"/>
      </Category>
      <Category ID="SouthAndCentralAmerica" title="Южная и Центральная Америка">
        <Category ID="SouthAmerica" title="Южная Америка">
          <Category ID="Argentina" title="Аргентина"/>
          <Category ID="Bolivia" title="Боливия"/>
          <Category ID="Brazil" title="Бразилия"/>
          <Category ID="Venezuela" title="Венесуэла"/>
          <Category ID="Guyana" title="Гайана"/>
          <Category ID="Colombia" title="Колумбия"/>
          <Category ID="Paraguay" title="Парагвай"/>
          <Category ID="Peru" title="Перу"/>
          <Category ID="Suriname" title="Суринам"/>
          <Category ID="Uruguay" title="Уругвай"/>
          <Category ID="Chile" title="Чили"/>
          <Category ID="Ecuador" title="Эквадор"/>
        </Category>
        <Category ID="CentralAmerica" title="Центральная Америка">
          <Category ID="Belize" title="Белиз"/>
          <Category ID="Guatemala" title="Гватемала"/>
          <Category ID="Honduras" title="Гондурас"/>
          <Category ID="CostaRica" title="Коста-Рика"/>
          <Category ID="Nicaragua" title="Никарагуа"/>
          <Category ID="Panama" title="Панама"/>
          <Category ID="ElSalvador" title="Сальвадор"/>
        </Category>
        <Category ID="Caribbean" title="Карибский бассейн">
          <Category ID="Anguilla" title="Ангилья"/>
          <Category ID="AntiguaAndBarbuda" title="Антигуа и Барбуда"/>
          <Category ID="Bahamas" title="Багамские острова"/>
          <Category ID="Barbados" title="Барбадос"/>
          <Category ID="Bermuda" title="Бермудские острова"/>
          <Category ID="BritishVirginIslands" title="Британские Виргинские острова"/>
          <Category ID="Haiti" title="Гаити"/>
          <Category ID="Guadeloupe" title="Гваделупа"/>
          <Category ID="Grenada" title="Гренада"/>
          <Category ID="Dominica" title="Доминика"/>
          <Category ID="DominicanRepublic" title="Доминиканская республика"/>
          <Category ID="Cuba" title="Куба"/>
          <Category ID="Martinique" title="Мартиника"/>
          <Category ID="NetherlandsAntilles" title="Нидерландские Антильские острова"/>
          <Category ID="PuertoRico" title="Пуэрто-Рико"/>
          <Category ID="SaintVincentAndGrenadines" title="Сент-Винсент и Гренадины"/>
          <Category ID="SaintKittsAndNevis" title="Сент-Китс и Невис"/>
          <Category ID="SaintLucia" title="Сент-Люсия"/>
          <Category ID="TurksAndCaicos" title="Теркс и Кайкос"/>
          <Category ID="TrinidadAndTobago" title="Тринидад и Тобаго"/>
          <Category ID="Jamaica" title="Ямайка"/>
        </Category>
      </Category>
    </Category>
    <Category ID="Africa" title="Африка">
      <Category ID="NorthernAfrica" title="Северная Африка">
        <Category ID="Algeria" title="Алжир">
          <Category ID="Algiers" title="Алжир"/>
        </Category>
        <Category ID="Egypt" title="Египет">
          <Category ID="Cairo" title="Каир"/>
          <Category ID="Alexandria" title="Александрия"/>
          <Category ID="Luxor" title="Луксор"/>
        </Category>
        <Category ID="Libya" title="Ливия"/>
        <Category ID="Mauritania" title="Мавритания">
          <Category ID="Nouakchott" title="Нуакшот"/>
        </Category>
        <Category ID="Morocco" title="Марокко"/>
        <Category ID="Sudan" title="Судан">
          <Category ID="Khartoum" title="Хартум"/>
        </Category>
        <Category ID="Tunisia" title="Тунис">
          <Category ID="Tunis" title="Тунис"/>
        </Category>
        <Category ID="SouthSudan" title="Южный Судан"/>
      </Category>
      <Category ID="WesternAfrica" title="Западная Африка">
        <Category ID="Benin" title="Бенин"/>
        <Category ID="BurkinaFaso" title="Буркина-Фасо"/>
        <Category ID="Gambia" title="Гамбия"/>
        <Category ID="Ghana" title="Гана"/>
        <Category ID="Guinea" title="Гвинея"/>
        <Category ID="GuineaBissau" title="Гвинея-Бисау"/>
        <Category ID="CapeVerde" title="Кабо-Верде"/>
        <Category ID="CoteDIvoire" title="Кот-д’Ивуар"/>
        <Category ID="Liberia" title="Либерия"/>
        <Category ID="Mali" title="Мали"/>
        <Category ID="Niger" title="Нигер"/>
        <Category ID="Nigeria" title="Нигерия"/>
        <Category ID="Senegal" title="Сенегал"/>
        <Category ID="SierraLeone" title="Сьерра-Леоне"/>
        <Category ID="Togo" title="Того"/>
      </Category>
      <Category ID="CentralAfrica" title="Центральная Африка">
        <Category ID="Angola" title="Ангола"/>
        <Category ID="Gabon" title="Габон"/>
        <Category ID="DRCongo" title="Демократическая Республика Конго"/>
        <Category ID="Cameroon" title="Камерун"/>
        <Category ID="Congo" title="Конго"/>
        <Category ID="SaoTomeAndPrincipe" title="Сан-Томе и Принсипи"/>
        <Category ID="CentralAfricanRepublic" title="ЦАР"/>
        <Category ID="Chad" title="Чад"/>
        <Category ID="EquatorialGuinea" title="Экваториальная Гвинея"/>
      </Category>
      <Category ID="EasternAfrica" title="Восточная Африка">
        <Category ID="Burundi" title="Бурунди"/>
        <Category ID="Djibouti" title="Джибути"/>
        <Category ID="Zambia" title="Замбия"/>
        <Category ID="Zimbabwe" title="Зимбабве"/>
        <Category ID="Kenya" title="Кения"/>
        <Category ID="Comoros" title="Коморы"/>
        <Category ID="Mauritius" title="Маврикий"/>
        <Category ID="Madagascar" title="Мадагаскар"/>
        <Category ID="Malawi" title="Малави"/>
        <Category ID="Mozambique" title="Мозамбик"/>
        <Category ID="Rwanda" title="Руанда"/>
        <Category ID="Seychelles" title="Сейшелы"/>
        <Category ID="Somalia" title="Сомали"/>
        <Category ID="Tanzania" title="Танзания"/>
        <Category ID="Uganda" title="Уганда"/>
        <Category ID="Eritrea" title="Эритрея"/>
        <Category ID="Ethiopia" title="Эфиопия"/>
      </Category>
      <Category ID="SouthernAfrica" title="Южная Африка">
        <Category ID="Botswana" title="Ботсвана"/>
        <Category ID="Lesotho" title="Лесото"/>
        <Category ID="Namibia" title="Намибия"/>
        <Category ID="Swaziland" title="Свазиленд"/>
        <Category ID="RSA" title="ЮАР"/>
      </Category>
    </Category>
    <Category ID="AustrOceania" title="Австралия и Океания">
      <Category ID="Australia" title="Австралия"/>
      <Category ID="NewZealand" title="Новая Зеландия"/>
      <Category ID="Oceania" title="Океания">
        <Category ID="Melanesia" title="Меланезия">
          <Category ID="Vanuatu" title="Вануату"/>
          <Category ID="NewCaledonia" title="Новая Каледония"/>
          <Category ID="PapuaNewGuinea" title="Папуа - Новая Гвинея"/>
          <Category ID="SolomonIslands" title="Соломоновы острова"/>
          <Category ID="Fiji" title="Фиджи"/>
        </Category>
        <Category ID="Micronesia" title="Микронезия">
          <Category ID="Kiribati" title="Кирибати"/>
          <Category ID="Nauru" title="Науру"/>
          <Category ID="Palau" title="Палау"/>
        </Category>
        <Category ID="Polynesia" title="Полинезия">
          <Category ID="CookIslands" title="Острова Кука"/>
          <Category ID="Pitcairn" title="Питкэрн"/>
          <Category ID="Samoa" title="Самоа"/>
          <Category ID="Tokelau" title="Токелау"/>
          <Category ID="Tonga" title="Тонга"/>
          <Category ID="Tuvalu" title="Тувалу"/>
          <Category ID="WallisAndFutuna" title="Уоллис и Футуна"/>
          <Category ID="FrenchPolynesia" title="Французская Полинезия"/>
        </Category>
      </Category>
    </Category>
    <Category ID="PolarRegions" title="Полярные области">
      <Category ID="Arctic" title="Арктика"/>
      <Category ID="Antarctic" title="Антарктика"/>
    </Category>
    <Category ID="Europe" title="Европа">
      <Category ID="WesternEurope" title="Западная Европа">
        <Category ID="Austria" title="Австрия"/>
        <Category ID="Belgium" title="Бельгия"/>
        <Category ID="GreatBritain" title="Великобритания">
          <Category ID="England" title="Англия"/>
          <Category ID="Wales" title="Уэльс"/>
          <Category ID="Scotland" title="Шотландия"/>
          <Category ID="NorthIreland" title="Северная Ирландия"/>
        </Category>
        <Category ID="Germany" title="Германия"/>
        <Category ID="Ireland" title="Ирландия"/>
        <Category ID="Liechtenstein" title="Лихтенштейн"/>
        <Category ID="Luxembourg" title="Люксембург"/>
        <Category ID="Monaco" title="Монако"/>
        <Category ID="Netherlands" title="Нидерланды"/>
        <Category ID="France" title="Франция"/>
        <Category ID="Switzerland" title="Швейцария"/>
      </Category>
      <Category ID="NorthernEurope" title="Северная Европа">
        <Category ID="Scandinavia" title="Скандинавия">
          <Category ID="Denmark" title="Дания"/>
          <Category ID="Iceland" title="Исландия"/>
          <Category ID="Norway" title="Норвегия"/>
          <Category ID="Finland" title="Финляндия"/>
          <Category ID="Sweden" title="Швеция"/>
        </Category>
        <Category ID="Baltic" title="Прибалтика">
          <Category ID="Latvia" title="Латвия"/>
          <Category ID="Lithuania" title="Литва"/>
          <Category ID="Estonia" title="Эстония"/>
        </Category>
      </Category>
      <Category ID="SouthernEurope" title="Южная Европа">
        <Category ID="Andorra" title="Андорра"/>
        <Category ID="Vatican" title="Ватикан"/>
        <Category ID="Spain" title="Испания"/>
        <Category ID="Italy" title="Италия"/>
        <Category ID="Malta" title="Мальта"/>
        <Category ID="Portugal" title="Португалия"/>
        <Category ID="SanMarino" title="Сан-Марино"/>
      </Category>
      <Category ID="SoutheasternEurope" title="Юго-Восточная Европа">
        <Category ID="Albania" title="Албания"/>
        <Category ID="Bulgaria" title="Болгария"/>
        <Category ID="BosniaAndHerzegovina" title="Босния и Герцеговина"/>
        <Category ID="Greece" title="Греция"/>
        <Category ID="Cyprus" title="Кипр"/>
        <Category ID="Macedonia" title="Македония"/>
        <Category ID="Serbia" title="Сербия">
          <Category ID="Kosovo" title="Косово"/>
        </Category>
        <Category ID="Slovenia" title="Словения"/>
        <Category ID="Croatia" title="Хорватия"/>
        <Category ID="Montenegro" title="Черногория"/>
        <Category ID="Turkey" title="Турция"/>
      </Category>
      <Category ID="EasternEurope" title="Восточная Европа">
        <Category ID="Hungary" title="Венгрия"/>
        <Category ID="Moldavia" title="Молдавия">
          <Category ID="Pridnestrovje" title="Приднестровье"/>
        </Category>
        <Category ID="Poland" title="Польша"/>
        <Category ID="Romania" title="Румыния"/>
        <Category ID="Slovakia" title="Словакия"/>
        <Category ID="CzechRepublic" title="Чехия"/>
        <Category ID="Belorussia" title="Белоруссия">
          <Category ID="MinskObl" title="Минск и область">
            <Category ID="Minsk" title="Минск"/>
          </Category>
          <Category ID="Brest" title="Брестская область"/>
          <Category ID="Vitebsk" title="Витебская область"/>
          <Category ID="Gomel" title="Гомельская область"/>
          <Category ID="Grodno" title="Гродненская область"/>
          <Category ID="Mogilev" title="Могилевская область"/>
        </Category>
        <Category ID="Ukraine" title="Украина">
          <Category ID="KievObl" title="Киев и область">
            <Category ID="Kiev" title="Киев"/>
          </Category>
          <Category ID="Vinnitsa" title="Винницкая область"/>
          <Category ID="Volyn" title="Волынская область"/>
          <Category ID="Dnepropetrovsk" title="Днепропетровская область"/>
          <Category ID="Donetsk" title="Донецкая область"/>
          <Category ID="Zhitomir" title="Житомирская область"/>
          <Category ID="Zakarpatye" title="Закарпатская область"/>
          <Category ID="Zaporozhye" title="Запорожская область"/>
          <Category ID="IvanoFrankovsk" title="Ивано-Франковская область"/>
          <Category ID="Kirovograd" title="Кировоградская область"/>
          <Category ID="Lugansk" title="Луганская область"/>
          <Category ID="Lvov" title="Львовская область"/>
          <Category ID="Nikolayev" title="Николаевская область"/>
          <Category ID="Odessa" title="Одесская область"/>
          <Category ID="Poltava" title="Полтавская область"/>
          <Category ID="Rovny" title="Ровенская область"/>
          <Category ID="Sumy" title="Сумская область"/>
          <Category ID="Ternopol" title="Тернопольская область"/>
          <Category ID="Kharkov" title="Харьковская область"/>
          <Category ID="Kherson" title="Херсонская область"/>
          <Category ID="Khmelnitsky" title="Хмельницкая область"/>
          <Category ID="Cherkassy" title="Черкасская область"/>
          <Category ID="Chernigov" title="Черниговская область"/>
          <Category ID="Cernovtsy" title="Черновицкая область"/>
        </Category>
      </Category>
      <Category ID="SouthCaucasus" title="Закавказье">
        <Category ID="Azerbaijan" title="Азербайджан">
          <Category ID="Nakhchivan" title="Нахичевань"/>
          <Category ID="Karabakh" title="Нагорный Карабах"/>
        </Category>
        <Category ID="Armenia" title="Армения"/>
        <Category ID="Georgia" title="Грузия">
          <Category ID="Abkhazia" title="Абхазия"/>
          <Category ID="SouthOssetia" title="Южная Осетия"/>
        </Category>
      </Category>
    </Category>
  </Category>
</CFDoc>

"""

ashmanow_world_strings = _world_xml.split("\n")

""" индексы элемента, начиная с нуля, -1 в случае отсутствия такого элемента"""
def tryind(needle, hay):
    try:
        return hay.index(needle)
    except:
        return -1

ashmanow_world_strings = [x for x in ashmanow_world_strings if (x.strip()<>"") and (x.strip()<>"</Category>")]
ashmanow_world_strings = [x[tryind("title=", x)+7:] for x in ashmanow_world_strings if tryind("title=", x)>0]
ashmanow_world_strings = [x[:tryind('"', x)].strip() for x in ashmanow_world_strings if tryind('"', x)>0]


if __name__ == '__main__':
    print "Testing ashmanow world"
    for el in ashmanow_world_strings:
        print el
        pass
    pass