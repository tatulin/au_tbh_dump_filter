# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time, os.path

from log_processor import * #LogProcessor

help_doc = u"""
Скрипт создает в дирректории data/temp_uids/ файлы, с названием файла= "uID", в котором собраны все датапоинты по одному uID
Структура файла такая же как у входного файла
файлы храняться в папке data/temp_uids/
"""


class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        #self.dps_users_dict = {} #{ numberOfDatapoints: [numberOfUser]}
    def process_tokens(self, tokens_list):
        new_folder_path = os.path.dirname(self.fn) + r"\temp_uids"
        if not os.path.isdir(new_folder_path):
            os.makedirs(new_folder_path)
        uID = tokens_list[0][0]
        new_file = new_folder_path + r"\%s.csv" % uID
        with utf_open_append(new_file) as out_f:
            for tokens in tokens_list:
                str = tokens
                out_f.write("\t".join(str) + "\r")
                pass


    def save_dict(self):
        pass

def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass

if __name__ == '__main__':
    main()
    pass