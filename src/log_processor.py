# -*- coding: UTF-8 -*-
import codecs
import sys, os, time, progress_printer

help_doc = u"""
Пропускает через себя лог, запускает аккумуляторы, сохраняет результат.
"""
def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

service_domains = set(["83.149.125.130"]) # блэк лист доменов

def utf_open_append(fn):
    f = codecs.open(fn, 'a', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

def ts():
    return time.strftime("%H:%M:%S", time.localtime())

class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass

class LogProcessor():
    def __init__(self):
        pass
    def _process_toks(self, tokens_list, accs):
        if len(tokens_list)==0:
            return
        if len(accs)==0:
            return
        for acc in accs:
            acc.process_tokens(tokens_list)
        pass

    def process_log(self, fn, accs):
        with utf_open_read(fn) as f:
            lc = 0
            uid = None
            limit = None
            accumulated_toks = []
            for line in f:
                lc+=1
                if lc%(100*1000) == 0:
                    print "{} Processed {}k lines".format(ts(), lc/1000)
                if limit and (lc>limit):
                    print "stopped on limit",lc, "lines"
                    break

                tokens = line.replace("\r", "").replace("\n", "").strip().split("\t")
                if  len(tokens)==17:
                    new_uid = tokens[0]
                    if new_uid<>uid:
                        self._process_toks(accumulated_toks, accs)
                        accumulated_toks = []
                        uid = new_uid
                    accumulated_toks.append(tokens)
            if len(accumulated_toks)>0:
                self._process_toks(accumulated_toks, accs)
                accumulated_toks = []

            print "Saving dictionaries..."
            for acc in accs:
                acc.save_dict()
pass


if __name__ == '__main__':
    print u""" Не надо запускать этот файл. Он нужен для запуска других, сам по себе ничего не делает."""