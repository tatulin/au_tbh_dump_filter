# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time, math, time


from log_processor import * #LogProcessor
help_doc = u"""
Скрипт фильтрует файл подающийся на вход и выводит отфильтрованый результ в новый файл.
Фильтрует по дублям, по вхождению домена в черный список(service_domains) и по наличию информации от ашманова(если ее нет то удаляет куку)

Выходной файл создается в той же директории с именем "имя_входного_файла"+"_filter_dubs.csv"

"""

class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass



def str_time_from_begin_era(tokens): #возвращает кол-во секунд с начала эрохи до даты поступившей на вход
    dt = time.strptime(tokens[4], "%Y-%m-%d %H:%M:%S")
    time_from_begin_era = time.mktime(dt)
    return time_from_begin_era

class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        out_fn  = fn+"_filter_dubs.csv"

        self.out_file = utf_open_append(out_fn)
        self.wo_double_dict = {} #

    def __del__(self):
        pass

    def process_tokens(self, tokens_list):
        lines_for_write = []
        for item in tokens_list:# запись в массив с фильтрацией по пустому ашманову и сайтам в черном списке(service_domains)
            domain = item[2].strip()
            ash = item[10].strip()
            url = item[1].strip()
            if url == "":
                continue
            if ash == "":
                continue
            if ash == "[]":
                continue
            if domain not in service_domains:
                lines_for_write.append(item)


        uniq_dps = {}

        for index, tokens in enumerate(lines_for_write):
            temp_key = tokens[1] + tokens[4]
            uniq_dps[temp_key] = tokens
        lines_for_write = uniq_dps.values()
        lines_for_write.sort(key=str_time_from_begin_era)

        lines_for_del = [] #создание списка с номерами строк для удаления
        for index, tokens in enumerate(lines_for_write): #цикл прохода по строкам файла с куками, с получением индекса строки
            url = tokens[1].strip()
            j_index = 1
            if index in lines_for_del:
                continue
            base_dtp_time = str_time_from_begin_era(tokens)
            for stack_tokens in lines_for_write[index + 1:]: #цикл по файлу с куками с заданной строки index
                if math.fabs(base_dtp_time - str_time_from_begin_era(stack_tokens)) <= 3: #сравнение по урлу, дате и врмемени
                    if url == stack_tokens[1].strip():
                        lines_for_del.append(index + j_index) # запись номера найденной строки в список
                else:
                    break
                j_index += 1
            pass
        pass
        lines_for_del = set(lines_for_del) #преобразование списка в множество, для удалению дубликатов




        for index, el in enumerate(lines_for_write):#запись отфильтрованного массива в файл
            if index not in lines_for_del:
                self.out_file.write("\t".join(el) + "\r")
        pass



    def save_dict(self):
        pass




def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print ts(), u"Закончено"

    pass

if __name__ == '__main__':
    main()
    time.sleep(3600)
    pass


