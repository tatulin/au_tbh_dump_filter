# -*- coding: UTF-8 -*-
import codecs
import sys, os, time
from ashmanow_geography import ashmanow_world_strings

from log_processor import * #LogProcessor
import json as json

help_doc = u"""
Составляет статистику по датапойнтам: сколько в какой интерес попало датапойнтов.
География исключена.
"""

"""
class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass
"""

class DpThemeAcc():
    def __init__(self, fn):
        self.themes = {}
        self.fn = fn
        pass

    def process_tokens(self, tokens_list):
        for tokens in tokens_list:
            ash_src = tokens[10]
            if ash_src == "[]":
                continue
            if ash_src[0]=='"':
                ash_src = ash_src[1:-1]
            ash_src = ash_src.replace('""', '"')


            ash = json.loads(ash_src)
            for el in ash:
                if u"name" not in el:
                    continue
                cat = el[u"name"]
                if cat in self.themes:
                    self.themes[cat] += 1
                else:
                    self.themes[cat] = 1
                #print cat
            pass
        pass

    def save_dict(self):
        save_fn = self.fn + "_datapoints_stat.csv"
        pairs = self.themes.items()
        pairs.sort(key = lambda x: -x[1])
        with utf_open_write(save_fn) as out_f:
            out_f.write( u"Тема\tКоличество датапойнтов с этой темой\r")
            for el in pairs:
                if el[0] in ashmanow_world_strings:
                    continue
                out_f.write( el[0] + "\t" + str(el[1]) + "\r")
            pass
        pass


def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = DpThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass

if __name__ == '__main__':
    main()
    pass