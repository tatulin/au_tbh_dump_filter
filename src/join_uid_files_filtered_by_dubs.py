# -*- coding: UTF-8 -*-
__author__ = 'vladislav'
import codecs
import sys, os, time, math, time, glob, progress_printer


from log_processor import * #LogProcessor

import filter_wo_double

help_doc = u"""
Скрипт фильтрует файлы, сгруппированыые по uID и хранящиеся в директории data/temp_uids, с помощью filter_wo_double.py и объеденяет файлы в один "data/temp_uids/combined.csv"
файл хранится в папке data/temp_uids/
"""

files = glob.glob(r"C:\git repositories\data\big_data\temp_uids\*.csv")
for file in files:
    if file.endswith("combined.csv"):
        continue
    if file.endswith("_filter_dubs.csv"):
        continue
    else:
        fn = file
        print "file name=", fn
        ta = filter_wo_double.ThemeAcc(fn)
        LogProcessor().process_log(fn, [ta] )
    pass


files = glob.glob(r"C:\git repositories\data\big_data\temp_uids\*_filter_dubs.csv")
combined_file = r"C:\git repositories\data\big_data\combined.csv"
with utf_open_append(combined_file) as out_file:
    count = len(files)
    i = 1
    pp = progress_printer.ProgressPrinter()
    for file in files:
        with utf_open_read(file) as cookie_file:
            pp.print_progress(i, count)
            out_file.write("".join(cookie_file))
        i += 1
        pass
# print u"время прошло=", time.clock()
# print u"файлов добавлено=", len(files)
print u"завершено"
