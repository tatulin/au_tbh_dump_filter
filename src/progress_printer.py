# -*- coding: UTF-8 -*-
import time, log_processor, sys, os

def ts():
    return time.strftime("%H:%M:%S", time.localtime())
help_doc = u"""Класс используется для вывода прогресса по обработке файла, используется в связке с log_processor
"""

class ProgressPrinter:
    def __init__(self):
        self.last_printed_time = time.localtime()

    def __del__(self):
        pass

    def print_progress(self, i, count):
        from_begin_era_date_time = time.mktime(self.last_printed_time)
        date_time_now = time.localtime()
        from_begin_era_date_time_now = time.mktime(date_time_now)
        if int(from_begin_era_date_time_now - from_begin_era_date_time) > 10:
            self.last_printed_time = date_time_now
            print time.strftime("%H:%M:%S", self.last_printed_time), u" выполнено {} из {}".format(str(i), str(count))
