# -*- coding: UTF-8 -*-
import codecs
import sys, os, time

help_doc = u"""
    Первым аргументом должен быть файл дампа.
    Из него будут получены два файла: в одном только датапойнты, разпознанные Ашмановым, в другом - нераспознанные Ашмановым датапойнты.

    При запуске без аргументов или с более, чем одним аргументом, будет выведено это сообщение.

    Ашманов должен быть на 11-м месте(одиннадцатая колонка).
    """



def utf_open_read(fn):
    f = codecs.open(fn, 'r', 'utf-8', errors="replace")
    return f

def utf_open_write(fn):
    f = codecs.open(fn, 'w', 'utf-8')
    return f

def main():
    if len(sys.argv)<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден.", fn
        return
    print u"Запущено разделание на датапойнты с Ашмановым и без."
    w_ash_fn = fn + "_w_ashmanow.csv"
    wo_ash_fn = fn + "_wo_ashmanow.csv"

    lc = 0
    with utf_open_read( fn) as f, utf_open_write(w_ash_fn) as w_a, utf_open_write(wo_ash_fn) as wo_a:
        for line in f:
            lc += 1
            if lc%100000==0:
                print lc/1000, u"k lines processed"
            tokens = line.strip().split("\t")
            if len(tokens)<>17:
                continue
            ash = tokens[10]
            if ash=="[]" or ash =='"[]"':
                wo_a.write(line)
            else:
                w_a.write(line)
    print "Процесс завершен. Строк пройдено: ", lc
    time.sleep(10)
    pass


if __name__ == '__main__':
    main()
    pass