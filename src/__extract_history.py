# -*- coding: UTF-8 -*-
import codecs
import sys, os, time

from log_processor import * #LogProcessor

help_doc = u"""
Выгружаем историю по заданым в параметрах кукам.
Если нет идентификаторов кук в аргументах - программа завершается.
"""

class ExtractorAcc:
    def __init__(self, fn, uids_to_extract):
        self.uids_to_extract = uids_to_extract #set([uid_1, uid_2])
        self.fn = fn
        self.stopped = False
        pass
    def process_tokens(self, tokens_list):
        uid = tokens_list[0][0]
        if uid in self.uids_to_extract:
            self.uids_to_extract -= set([uid])
            with utf_open_write(self.fn + "_" + uid +".tsv") as out_f:
                for toks in tokens_list:
                    out_f.write( '\t'.join(toks) + "\r")
            pass
        if len(self.uids_to_extract) == 0:
            print "Все куки выгружены. Аварийно завершаем все  и вся :)"
            self.stopped = True
            basta #how to stop without refactoring anyway?
        pass
    def save_dict(self):
        #данные и так сохраняются на каждой итерации
        pass

def main():
    len_arg = len(sys.argv)
    if len_arg<2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    uids = set( sys.argv[2:] )

    ta = ExtractorAcc(fn, uids)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass
    pass

if __name__ == '__main__':
    main()
    pass