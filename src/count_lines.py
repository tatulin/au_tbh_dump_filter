# -*- coding: UTF-8 -*-
import sys, time


def c_lines():
    if len(sys.argv)<>2:
        print u"Введите аргумент командной строки - имя файла в котором надо подсчитать строчки."

        return
    fn = sys.argv[1]
    print u"Идет подсчет строк..."
    with open(fn) as f:
        lc = 0
        for line in f:
            lc+=1
            if lc%(10**6)==0:
                print lc/(10**6), u"млн строк... идет подсчет... "
    pass
    print u"всего строк:", lc
    print u"Через 10 секунд работа скрипта будет завершена."
    time.sleep(10)

if __name__ == '__main__':

    c_lines()
    pass