# -*- coding: UTF-8 -*-
import codecs
import sys, os, time

from log_processor import * #LogProcessor

help_doc = u"""
Скрипт составляет словарь доменов по посещаемости.
Выводит данные ввиде: домен - уникальные посетители - датапойнты - кол-во датапойнтов с тематикой

Ашманова:
А) Новости
Б) Эротика и секс; Эротика, порнография; Брань
В) Игры


"""

"""
class BaseAccClass():
    def process_tokens(self, tokens_list):
        pass
    def save_dict(self):
        pass
"""


class ThemeAcc:
    def __init__(self, fn):
        self.fn = fn
        self. d = {}
        self.domain_uids_dps = {} #{ domain: [uids, dps, news, ero, Games]}
    def process_tokens(self, tokens_list):
        all_user_domains = {} #domain: urls_visited
        for tokens in tokens_list:
            domain = tokens[2]

            if domain.strip()=="":
                continue
            if domain not in all_user_domains:
                all_user_domains[domain]=1
            else:
                all_user_domains[domain]+=1

            if domain not in self.domain_uids_dps:
                self.domain_uids_dps[domain]=[0, 1, 0, 0, 0]
            else:
                self.domain_uids_dps[domain][1]+=1

            ash =  tokens[10]
            if ash==u"[]":
                continue
            if u"Новости" in ash:
                self.domain_uids_dps[domain][2]+=1

            if u"Эротика" in ash:
                self.domain_uids_dps[domain][3]+=1
            if u"Порнография" in ash:
                self.domain_uids_dps[domain][3]+=1
            if u"Брань" in ash:
                self.domain_uids_dps[domain][3]+=1

            if u"Игры" in ash:
                self.domain_uids_dps[domain][4]+=1
        pass

        """ Уникальные пользователи по домену"""
        for d in all_user_domains:
            self.domain_uids_dps[d][0]+=1
        pass

    def save_dict(self):
        out_fn = self.fn + "_ThemeStat.csv"
        du = self.domain_uids_dps.items()
        du.sort(key=lambda x: -float(x[1][0]))
        with utf_open_write(out_fn) as out_f:
            out_f.write('\t'.join(['domain','uids','visits' ,'News','Ero','Games']) + '\r')
            for el in du:
                new_line = "\t".join([el[0], str(el[1][0]), str(el[1][1]), str(el[1][2]), str(el[1][3]), str(el[1][4])])
                out_f.write(new_line + "\r")
            pass
        pass


def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    ta = ThemeAcc(fn)
    LogProcessor().process_log(fn, [ta] )
    print u"Закончено"
    time.sleep(30)
    pass

if __name__ == '__main__':
    main()
    pass