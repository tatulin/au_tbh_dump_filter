# -*- coding: UTF-8 -*-
import codecs
import sys, os, time
import cPickle


sys.path.append("quality_matrix/")
from mongo_url_acc import UrlAshAcc

from log_processor import * #LogProcessor

"""
План
В несколько потоков обрабатывать разные файлы.

Составляем словари  домен : [url, ash_src]

Влезут ли URL в оперативку?

"""

help_doc = u"""
    параметры не заданы

    Требуется, чтобы была запущена mongoDB на стандатном порту. Допускается запустить ее не сервисом, а просто из командной строки.


"""



class ThemeCats:
    def __init__(self):
        self.porn_cats = [["Игры"], #6
            ["Эротика и секс", "Порно", "Порнография", "Брань"],#7
            ["Происшествия", "Общество"]]#8
        self.good_cats = [["Авто, мото"],
            ["Здоровье и красота"],
            ["Недвижимость", "Строительство", "Ремонт"]]
    pass

class Res:
    pass

def collect_stat(url_ash_acc):
    col = url_ash_acc.no_ash_urls_collection
    res_urls_wo_ash = col.count()

    col = url_ash_acc.ash_urls_collection
    res_urls_w_ash = col.count()

    report = []
    report.append( "\t".join(["Тип данных                 ", \
                         "Доменов","% от всех доменов", "Страниц", "% от всех страниц", \
                         "Уников", "% от всех уников", "Датапойнтов","% от всех датапойнтов" ])    )
    report.append( "\t".join(["1 Все данные", \
                         "", "", str(res_urls_wo_ash+ res_urls_w_ash), "100.0 %",\
                         "", "", "", ""])    )
    url_pc = 100 * float(res_urls_wo_ash)/(res_urls_wo_ash+ res_urls_w_ash)
    url_pc = "%.1f " % url_pc + "%"
    report.append( "\t".join(["2 Данные без категорий Ашманова", \
                         "-", "-", str(res_urls_wo_ash), url_pc,\
                         "", "", "", ""])    )
    url_pc = 100 * float(res_urls_w_ash)/(res_urls_wo_ash+ res_urls_w_ash)
    url_pc = "%.1f " % url_pc + "%"
    report.append( "\t".join(["3 Данные с категориями Ашманова", \
                         "-", "-", str( res_urls_w_ash), url_pc,\
                         "", "", "", ""])    )

    report.append("")
    report.append( "\t".join(["4 Данные с  полезными категориями (без категорий пп. 6-8)", \
                         "-", "-", str( res_urls_w_ash), url_pc,\
                         "", "", "", ""])    )
    report.append( "\t".join(["5 Данные с целевыми категориями (из пп. 9-12", \
                         "-", "-", str( res_urls_w_ash), url_pc,\
                         "", "", "", ""])    )

    for line in report:
        print line



    pass

def main():
    len_arg = len(sys.argv)
    if len_arg<>2:
        print help_doc
        time.sleep(10)
        return
    fn = sys.argv[1]
    if not os.path.isfile(fn):
        print u"Файл не найден", fn
        time.sleep(10)
        return
    url_ash = UrlAshAcc(fn, ThemeCats())
    LogProcessor().process_log(fn, [url_ash] )

    collect_stat(url_ash)

    print u"Закончено"
    time.sleep(30)
    pass


if __name__ == '__main__':
    main()
    pass